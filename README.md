# Bases POO - Les Collections

## Exercice 1 - package ex1
1. Exécutez la fonction main
2. Observez le résultat de la fonction list_1 et celui de la fonction list_2 : dans quel ordre ont été affichées les va
   valeurs ?
3. Observez le résultat de la fonction map_1 et celui de la fonction map_2 : dans quel ordre ont été affichées les va
   valeurs ?
4. Une collection est dite "ordonnée" si elle conserve l'ordre dans lequel les éléments sont introduits dans la collection

   a. Le type List est-il ordonné ou non-ordonné ?

   b. Le type Map est-il ordonné ou non-ordonné ?

## Exercice 2 - package ex2
Nous définissons deux classes : VoitureList et VoitureMap
1. Quelle est la différence entre ces deux classes ?
2. Observez le constructeur de chaque classe : en quoi est-il différent des constructeur observés jusqu'ici ?
   Déduisez-en une règle pour les classes contenant des collections
3. En vous aidant de ces classes et de l'exercice 1 :

   a. Expliquez comment parcourir un ArrayList en Java

   b. Expliquez deux manières de parcourir un HashMap en Java

4. Observez la ligne 14 de la classe VoitureList et la ligne 15 de la classe VoitureMap.
   Observez aussi les suggestions d'IntelliJ pour les lignes 18, 33, 48 et 63 de la classe Main de l'exercice 1.
   Déduisez-en une règle pour l'utilisation de la notation diamant en Java.