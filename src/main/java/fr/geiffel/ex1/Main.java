package fr.geiffel.ex1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        list_1();
        list_2();
        map_1();
        map_2();
    }

    public static void list_1() {
        System.out.println("list_1 : ");
        List<Integer> nombres = new ArrayList<Integer>();
        nombres.add(2);
        nombres.add(6);
        nombres.add(4);
        nombres.add(5);
        nombres.add(3);
        nombres.add(1);

        for(Integer valeur : nombres) {
            System.out.println(valeur);
        }
    }

    public static void list_2() {
        System.out.println("list_2 : ");
        List<Integer> nombres = new ArrayList<Integer>();
        nombres.add(6);
        nombres.add(5);
        nombres.add(4);
        nombres.add(1);
        nombres.add(3);
        nombres.add(2);

        for(Integer valeur : nombres) {
            System.out.println(valeur);
        }
    }

    public static void map_1() {
        System.out.println("map_1 : ");
        Map<Integer, Integer> nombres = new HashMap<Integer, Integer>();
        nombres.put(2,6);
        nombres.put(1,2);
        nombres.put(3,4);
        nombres.put(4,5);
        nombres.put(5,3);
        nombres.put(6,1);

        for(Integer valeur : nombres.values()) {
            System.out.println(valeur);
        }
    }

    public static void map_2() {
        System.out.println("map_2 : ");
        Map<Integer, Integer> nombres = new HashMap<Integer, Integer>();
        nombres.put(6,1);
        nombres.put(3,4);
        nombres.put(2,6);
        nombres.put(5,3);
        nombres.put(1,2);
        nombres.put(4,5);

        for(Integer valeur : nombres.values()) {
            System.out.println(valeur);
        }
    }
}