package fr.geiffel.ex2;

import java.util.ArrayList;
import java.util.List;

public class VoitureList {
    private String nom;
    private String couleur;
    private List<Roue> roues;

    public VoitureList(String nom, String couleur) {
        this.nom = nom;
        this.couleur = couleur;
        this.roues = new ArrayList<>();
    }

    //Methodes
    public Roue getRoue(int index) {
        return this.roues.get(index);
    }
    public void addRoue(Roue roue) {
        this.roues.add(roue);
    }

    public void rouler(int nbKilometres) {
        boolean pasDeRouesCrevees = true;
        int i = 0;
        while (pasDeRouesCrevees && i<4) {
            Roue laRoue = this.roues.get(i);
            pasDeRouesCrevees &= !laRoue.estCrevee();
            if(laRoue.estCrevee()) {
                System.out.println("La roue "+i+" est crevée !");
            }
            i++;
        }
        if (pasDeRouesCrevees) {
            System.out.println(this.nom+" roule pendant "+nbKilometres+" kilometres");
        } else {
            System.out.println("Il y a des roues crevées, "+this.nom+" ne peut pas rouler !");
        }
    }

    //Getters
    public String getNom() {
        return nom;
    }
    public String getCouleur() {
        return couleur;
    }
    public List<Roue> getRoues() {
        return roues;
    }

    //Setters
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }
    public void setRoues(List<Roue> roues) {
        this.roues = roues;
    }
}
