package fr.geiffel.ex2;

public class Roue {
    private Boolean crevee;

    public Roue() {
        //Par défaut une roue est fonctionnelle
        this.crevee = true;
    }

    public Roue(Boolean crevee) {
        this.crevee = crevee;
    }

    //Exception à la règle des getters : on choisit de changer
    //le nom de la méthode pour la rendre plus expressive
    public Boolean estCrevee() {
        return !this.crevee;
    }

    public void setCrevee(Boolean crevee) {
        this.crevee = !crevee;
    }
}
