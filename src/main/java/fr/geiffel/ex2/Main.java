package fr.geiffel.ex2;

public class Main {
    public static void main(String[] args) {
        VoitureList voitureList = new VoitureList("Titine", "bleue");
        VoitureMap voitureMap = new VoitureMap("Dedeuche", "jaune");

        voitureList.addRoue(new Roue());
        voitureList.addRoue(new Roue());
        voitureList.addRoue(new Roue());
        voitureList.addRoue(new Roue());

        voitureMap.addRoue("avantGauche", new Roue());
        voitureMap.addRoue("avantDroite", new Roue());
        voitureMap.addRoue("arriereGauche", new Roue());
        voitureMap.addRoue("arriereDroite", new Roue());

        System.out.println("VoitureList ok :");
        voitureList.rouler(10);

        System.out.println("VoitureMap ok :");
        voitureMap.rouler(10);

        voitureList.getRoue(1).setCrevee(true);
        System.out.println("VoitureList pas ok :");
        voitureList.rouler(10);

        voitureMap.getRoue("avantDroite").setCrevee(true);
        System.out.println("VoitureMap pas ok :");
        voitureMap.rouler(10);
    }
}
