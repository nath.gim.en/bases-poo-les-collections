package fr.geiffel.ex2;

import java.util.HashMap;
import java.util.Map;

public class VoitureMap {

    private String nom;
    private String couleur;
    private Map<String, Roue> roues;

    public VoitureMap(String nom, String couleur) {
        this.nom = nom;
        this.couleur = couleur;
        this.roues = new HashMap<>();
    }

    //Methodes
    public Roue getRoue(String nom) {
        return this.roues.get(nom);
    }
    public void addRoue(String nom, Roue roue) {
        this.roues.put(nom, roue);
    }

    public void rouler(int nbKilometres) {
        boolean pasDeRouesCrevees = true;
        int i = 0;
        for(String nom : this.roues.keySet()) {
            Roue laRoue = this.roues.get(nom);
            pasDeRouesCrevees &= !laRoue.estCrevee();
            if (laRoue.estCrevee()) {
                System.out.println("La roue "+nom+" est crevée !");
            }
            i++;
        }
        if (pasDeRouesCrevees) {
            System.out.println(this.nom+" roule pendant "+nbKilometres+" kilometres");
        } else {
            System.out.println("Il y a des roues crevées, "+this.nom+" ne peut pas rouler !");
        }
    }

    public void creverRoue(String nom) {
        this.roues.get(nom).setCrevee(true);

    }

    //Getters
    public String getNom() {
        return nom;
    }
    public String getCouleur() {
        return couleur;
    }
    public Map<String, Roue> getRoues() {
        return roues;
    }

    //Setters
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }
    public void setRoues(Map<String, Roue> roues) {
        this.roues = roues;
    }
}
